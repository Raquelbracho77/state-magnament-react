import React, {useReducer} from 'react';

const reducer = (state, action) => {
	switch(action.type) {
		case 'buttonClick':
			return {...state, count: state.count +1};
		default:
			return state;
	}
};
const App = () => {
	let [state, dispatch] = useReducer(reducer, {count: 0});
	return (
		<div>
			<button onClick={() => dispatch({type: 'buttonClick'})}>
				Click me
			</button>
			current count is {state.count}
		</div>
	);
};

export default App;